﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClipboardFormatter.Properties;
using EventHook;

namespace ClipboardFormatter
{
    public partial class FormMain : Form
    {
        private bool _dotNewLine = false;
        private static string _currentStr = "";
        public FormMain()
        {
            InitializeComponent();
            chkDotEnd.Checked = _dotNewLine;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void Start()
        {
            ClipboardWatcher.Start();
            lblState.ForeColor = Color.Green;
            lblState.Text = "ON";
            this.Text = "[On] Clipboard";
            notifyIcon1.Text = Text;
            notifyIcon1.Icon = Resources.ClipboardON;
            ClipboardWatcher.OnClipboardModified += ClipboardWatcherOnOnClipboardModified;
        }

        private async void ClipboardWatcherOnOnClipboardModified(object sender, ClipboardEventArgs clipboardEventArgs)
        {
            if (clipboardEventArgs.DataFormat != ClipboardContentTypes.PlainText &&
                clipboardEventArgs.DataFormat != ClipboardContentTypes.UnicodeText) return;
            {
                string s = clipboardEventArgs.Data as string;

                if (!string.IsNullOrEmpty(s))
                {
                    if (!string.IsNullOrWhiteSpace(_currentStr) && s.StartsWith(_currentStr))
                    {
                        return;
                    }
                    await Process(s);
                }
            }
        }

        private async Task Process(string s)
        {
            _currentStr = s.Substring(0, s.Length > 1000 ? 1000 : s.Length);

            var ss = s.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder sb = new StringBuilder();
            foreach (var str in ss)
            {
                sb.Append(str);
                if (str.EndsWith("."))
                {
                    if (_dotNewLine)
                    {
                        sb.AppendLine();
                    }
                }
                else
                {
                    if (!str.EndsWith("-"))
                    {
                        sb.Append(" ");
                    }
                    else
                    {
                        sb.Remove(sb.Length - 1, 1);
                    }
                }
            }
            Clipboard.SetText(sb.ToString());
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void Stop()
        {
            ClipboardWatcher.Stop();
            lblState.ForeColor = Color.Crimson;
            lblState.Text = "OFF";
            this.Text = "[Off] Clipboard";
            notifyIcon1.Text = Text;
            notifyIcon1.Icon = Resources.Clipboard;
        }

        private void chkDotEnd_CheckedChanged(object sender, EventArgs e)
        {
            _dotNewLine = chkDotEnd.Checked;
        }

        private void notifyIcon1_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
                this.Activate();
                return;
            }
            if (ClipboardWatcher._IsRunning)
            {
                Stop();
            }
            else
            {
                Start();
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Close();
            }
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Stop();
        }

        private void FormMain_SizeChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }
    }
}
